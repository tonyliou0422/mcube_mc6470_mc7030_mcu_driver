/*****************************************************************************
 *
 * Copyright (c) 2017 mCube, Inc.  All rights reserved.
 *
 * This source is subject to the mCube Software License.
 * This software is protected by Copyright and the information and source code
 * contained herein is confidential. The software including the source code
 * may not be copied and the information contained herein may not be used or
 * disclosed except with the written permission of mCube Inc.
 *
 * All other rights reserved.
 *****************************************************************************/
/**
 * @file    m_drv_i2c.c
 * @author  mCube
 * @date    25 May 2017
 * @brief   C file of I2C driver interface based on Nordic nrf52832.
 * @see     http://www.mcubemems.com
 */

/* platform function includes */
#include "m_drv_i2c.h"

#include <zephyr/drivers/i2c.h>
#define I2C0_NODE DT_NODELABEL(mc6470)
static const struct i2c_dt_spec dev_i2c = I2C_DT_SPEC_GET(I2C0_NODE);

void M_DRV_I2C_Init(void)
{
	//platform bus initilization
   if (!device_is_ready(dev_i2c.bus)) {
      printk("I2C bus %s is not ready!\n\r",dev_i2c.bus->name);
      return;
   }
   printk("I2C bus %s is ready!\n\r",dev_i2c.bus->name);
}

int M_DRV_I2C_Write(uint8_t bI2CAddr, uint8_t bRegAddr, uint8_t *pbRegDataBuf, uint8_t bLength)
{
   //platform write function
   uint8_t data[bLength + 1];

   data[0] = bRegAddr;
   memcpy(&data[1], pbRegDataBuf, bLength);

   if (i2c_write_dt(&dev_i2c, data, bLength + 1) != 0)
   {
      printk("i2c write failed\n");
      return -1;
   }

   return E_M_DRV_MCUBE_RATCODE_SUCCESS;
}

int M_DRV_I2C_Read(uint8_t bI2CAddr, uint8_t bRegAddr, uint8_t *pbDataBuf, uint8_t bLength)
{
   //platform read function
   if (i2c_write_dt(&dev_i2c, &bRegAddr, 1) != 0)
   {
      printk("i2c read failed\n");
      return -1;
   }

   if (i2c_read_dt(&dev_i2c, pbDataBuf, bLength) != 0)
   {
      printk("i2c read failed\n");
      return -1;
   }

   return E_M_DRV_MCUBE_RATCODE_SUCCESS;   
}
